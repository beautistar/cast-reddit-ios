//
//  ActionSheetFilter.h
//  Cast Reddit
//
//  Created by praveen on 3/8/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FilterDelegate <NSObject>
@optional
- (void)filterSelecatedType:(NSString *)type;
 
@end


@interface ActionSheetFilter : UIViewController
@property (nonatomic, weak) id <FilterDelegate> delegate;
@property (weak, nonatomic) IBOutlet UISwitch *ibVideoSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *ibGifSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *ibPicSwitch;

@end
