//
//  ActionSheetSorting.m
//  Cast Reddit
//
//  Created by praveen on 3/8/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import "ActionSheetSorting.h"

@interface ActionSheetSorting ()

@end

@implementation ActionSheetSorting

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(IBAction)didTapOnCancelSheet:(id)sender{
  [self.view removeFromSuperview];
}

- (IBAction)didTapHotVideosButton:(id)sender {
    if (self.delegate) {
        [self.view removeFromSuperview];
        [self.delegate didTapOnVideoType:@"hot"];
    }
}

- (IBAction)didTapNewVideosButton:(id)sender {
    if (self.delegate) {
        [self.view removeFromSuperview];
        [self.delegate didTapOnVideoType:@"new"];
    }
}

- (IBAction)didTapTopVideosButton:(id)sender {
    if (self.delegate) {
        [self.view removeFromSuperview];
        [self.delegate didTapOnVideoType:@"top"];
    }
}
- (IBAction)didTapControversialVideosButton:(id)sender {
    if (self.delegate) {
        [self.view removeFromSuperview];
        [self.delegate didTapOnVideoType:@"controversial"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
