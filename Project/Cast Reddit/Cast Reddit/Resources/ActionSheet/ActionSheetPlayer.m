//
//  ActionSheetPlayer.m
//  Cast Reddit
//
//  Created by praveen on 3/8/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import "ActionSheetPlayer.h"

@interface ActionSheetPlayer ()

@end

@implementation ActionSheetPlayer

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(IBAction)didTapOnCancelSheet:(id)sender{
    [self.view removeFromSuperview];
}

- (IBAction)didTapPlayNow:(id)sender {
    if (self.delegate) {
        [self.view removeFromSuperview];
        [self.delegate didTapPlayVideo];
    }
}
- (IBAction)didTapOnAddtoQ:(id)sender {
    if (self.delegate) {
        [self.view removeFromSuperview];
        [self.delegate didTapOnAddtoQ];
    }
}
- (IBAction)didTapOnPlayNext:(id)sender {
    if (self.delegate) {
        [self.view removeFromSuperview];
        [self.delegate didTapOnPlayNext];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
