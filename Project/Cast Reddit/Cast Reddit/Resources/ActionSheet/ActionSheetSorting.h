//
//  ActionSheetSorting.h
//  Cast Reddit
//
//  Created by praveen on 3/8/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ActionSheetSortingDelegate <NSObject>
@optional
- (void)didTapOnVideoType:(NSString *)type;
@end

@interface ActionSheetSorting : UIViewController
@property (nonatomic, weak) id <ActionSheetSortingDelegate> delegate;

@end
