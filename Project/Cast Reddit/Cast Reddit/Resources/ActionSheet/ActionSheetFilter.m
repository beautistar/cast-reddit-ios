//
//  ActionSheetFilter.m
//  Cast Reddit
//
//  Created by praveen on 3/8/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import "ActionSheetFilter.h"

@interface ActionSheetFilter (){
 
}

@end

@implementation ActionSheetFilter

- (void)viewDidLoad {
    [super viewDidLoad];
 
}

-(IBAction)didTapOnCancelSheet:(id)sender{
    
    [self.view removeFromSuperview];
}
- (IBAction)didTapOnVideoSwitch:(id)sender {
    if (![_ibGifSwitch isOn] & ![_ibPicSwitch isOn]) {
        [_ibVideoSwitch setOn:YES];
        return;
    }
    
    if ([_ibVideoSwitch isOn]) {
        [_ibGifSwitch setOn:NO];
        [_ibPicSwitch setOn:NO];
        [self.delegate filterSelecatedType: @"videos"];
    }
    
}

- (IBAction)didTapOnGifSwitch:(id)sender {
    if (![_ibVideoSwitch isOn] & ![_ibPicSwitch isOn]) {
        [_ibGifSwitch setOn:YES];
        return;
    }
    if ([_ibGifSwitch isOn]) {
        [_ibPicSwitch setOn:NO];
        [_ibVideoSwitch setOn:NO];
        [self.delegate filterSelecatedType: @"gifs"];
    }
}

- (IBAction)didTapOnPicsSwitch:(id)sender {
    if (![_ibVideoSwitch isOn] & ![_ibGifSwitch isOn]) {
        [_ibPicSwitch setOn:YES];
        return;
    }
    if ([_ibPicSwitch isOn]) {
        [_ibGifSwitch setOn:NO];
        [_ibVideoSwitch setOn:NO];
        [self.delegate filterSelecatedType: @"pics"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
