//
//  ActionSheetPlayer.h
//  Cast Reddit
//
//  Created by praveen on 3/8/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ActionSheetPlayerDelegate <NSObject>
@optional
-(void)didTapPlayVideo;
-(void)didTapOnAddtoQ;
-(void)didTapOnPlayNext;
@end

@interface ActionSheetPlayer : UIViewController
@property (nonatomic, weak) id <ActionSheetPlayerDelegate> delegate;
@end
