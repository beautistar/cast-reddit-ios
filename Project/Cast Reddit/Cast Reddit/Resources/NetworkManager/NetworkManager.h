//
//  NetworkManager.h
//  Cast Reddit
//
//  Created by praveen on 1/21/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NetworkManager : NSObject

+ (instancetype)sharedInstance;

-(void)performNetworkOperationsWith:(NSDictionary *)paramter
                         andBaseUrl:()url
                        showLoading:(BOOL)isShow
                            success:(void (^)(NSDictionary *))success
                            failure:(void (^)(NSError *))failure;
@end
