//
//  NetworkManager.m
//  Cast Reddit
//
//  Created by praveen on 1/21/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import "NetworkManager.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

NSString *const BASE_URL = @"https://www.reddit.com/r";

@implementation NetworkManager

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static NetworkManager *instance;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    
    return instance;
}

- (instancetype)init {
    self = [super init];
    return self;
}

-(void)performNetworkOperationsWith:(NSDictionary *)paramter
                      andBaseUrl:(NSString *)url
                     showLoading:(BOOL)isShow
                         success:(void (^)(NSDictionary *))success
                         failure:(void (^)(NSError *))failure{

    AppDelegate *delegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    MBProgressHUD *hud;
    if (isShow) {
        hud = [MBProgressHUD showHUDAddedTo:delegate.window animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.animationType = MBProgressHUDAnimationZoomIn;
        hud.label.text = @"Please wait...";
       // hud.backgroundColor = [UIColor blueColor];
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
   // manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (hud) {
            [hud hideAnimated:YES];
        }
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (hud) {
            [hud hideAnimated:YES];
        }
        NSLog(@"error %@", error);
        failure(error);
    }];   
}


@end
