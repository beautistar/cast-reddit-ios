//
//  CRListVideoVC.m
//  Cast Reddit
//
//  Created by praveen on 1/16/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import "CRListVideoVC.h"
#import "CRVideoCell.h"
#import "QuartzCore/QuartzCore.h"
#import "MMAlertView.h"

@interface CRListVideoVC ()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation CRListVideoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[UISlider appearance] setThumbImage:[UIImage imageNamed:@"sliderThumb"]
                                forState:UIControlStateNormal];
    
    self.ibScrollLbl.marqueeType = MLContinuous;
    self.ibScrollLbl.scrollDuration = 10.0;
    self.ibScrollLbl.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    self.ibScrollLbl.fadeLength = 10.0f;
    self.ibScrollLbl.leadingBuffer = 0.0f;
    self.ibScrollLbl.trailingBuffer = 0.0f;
    NSString *strings = @"Videos | GIFS | Pics | Favorties | Subreddit  ";
    self.ibScrollLbl.text = strings;
    
}
- (IBAction)didTapOnconnectChromeCast:(id)sender {
    NSArray *items =@[MMItemMake(@"Disconnect", MMItemTypeNormal, nil),
                      MMItemMake(@"Connect", MMItemTypeNormal, nil)];
    
    [[[MMAlertView alloc] initWithTitle:@"Chromecast"
                                 detail:@"Do you want connect chromecost?"
                                  items:items]
     showWithBlock:nil];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CRVideoCell *cell = (CRVideoCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    return cell;
}


- (IBAction)didTapOnMoreBtn:(id)sender {
  
}
- (IBAction)didTapOnBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
        viewController = viewController.presentedViewController;
    }
    
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:actionSheet.view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationLessThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:viewController.view.frame.size.height*0.5f];
    [actionSheet.view addConstraint:constraint];
    
    
    //https://stackoverflow.com/questions/44601692
    [actionSheet.view setTintColor:[UIColor whiteColor]];
    UIView *firstSubview = actionSheet.view.subviews.firstObject;
    UIView *alertContentView = firstSubview.subviews.firstObject;
    for (UIView *subSubView in alertContentView.subviews) {
        subSubView.backgroundColor = [UIColor darkGrayColor];
    }
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Pay Now" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Pay Next" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Remove" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Move to Top" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Move to Bottom" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Move up 1" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Distructive button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Move Down 1" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Distructive button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Distructive button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)didTapOnHomeBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 

@end
