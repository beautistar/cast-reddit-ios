// Copyright 2016 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#import "MediaQueueViewController.h"

#import <GoogleCast/GoogleCast.h>

#import "AppDelegate.h"
#import "MediaItem.h"
#import "MediaListModel.h"

@interface MediaQueueViewController ()< UITableViewDataSource, UITableViewDelegate, GCKSessionManagerListener,
    GCKRemoteMediaClientListener, GCKRequestDelegate> {
        NSTimer *_timer;
        __weak IBOutlet UIButton *_editButton;
        
        // Queue
        IBOutlet UITableView *_tableView;
        
        // Queue/editing state.
        //  IBOutlet UIButton *_editButton;
        BOOL _editing;
        
        GCKRemoteMediaClient *_mediaClient;
        GCKUIMediaController *_mediaController;
        GCKRequest *_queueRequest;
        
        
        MediaItem *selectedItem;
        NSInteger selectedIndex;
        NSString *selecatedMediaTitle;
}
@property(nonatomic) MediaListModel *mediaList;

@end

@implementation MediaQueueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
    _editing = NO;
    
    self.ibScrollLbl.marqueeType = MLContinuous;
    self.ibScrollLbl.scrollDuration = 10.0;
    self.ibScrollLbl.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    self.ibScrollLbl.fadeLength = 10.0f;
    self.ibScrollLbl.leadingBuffer = 0.0f;
    self.ibScrollLbl.trailingBuffer = 0.0f;
    NSString *strings = @"Videos | GIFS | Pics | Favorties | Subreddit  ";
    self.ibScrollLbl.text = strings;
    
    CGRect thumbRect = CGRectMake(0,0,1,1); UIGraphicsBeginImageContextWithOptions(CGSizeMake(1,1), false, 0);
    [[UIColor clearColor] setFill]; UIRectFill(thumbRect);
    UIImage* blankImg = UIGraphicsGetImageFromCurrentImageContext(); UIGraphicsEndImageContext( );
    [self.ibSeekSlider setThumbImage:blankImg forState:UIControlStateNormal];
    
    GCKSessionManager *sessionManager = [GCKCastContext sharedInstance].sessionManager;

    [sessionManager addListener:self];
    if (sessionManager.hasConnectedCastSession) {
        [self attachToCastSession:sessionManager.currentCastSession];
    }
    
    UILongPressGestureRecognizer *recognizer =
    [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                  action:@selector(handleLongPress:)];
    
    recognizer.minimumPressDuration = 2.0;  // 2 seconds
    [_tableView addGestureRecognizer:recognizer];
    _tableView.separatorColor = [UIColor clearColor];
    

}
- (IBAction)didTapONBackButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    appDelegate.castControlBarsEnabled = NO;
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  _queueRequest = nil;
  _tableView.userInteractionEnabled = YES;
 
  if ([_mediaClient.mediaStatus queueItemCount] == 0) {
    [_editButton setEnabled:NO];
  } else {
    [_editButton setEnabled:YES];
  }

  [_tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
}

#pragma mark - UI Actions

- (IBAction)toggleEditing:(id)sender {
  if (_editing) {
    [_editButton setTitle:@"Edit" forState:UIControlStateNormal];
    [_tableView setEditing:NO animated:YES];
    _editing = NO;
    if ([_mediaClient.mediaStatus queueItemCount] == 0) {
      [_editButton setEnabled:NO];
    }
  } else {
    [_editButton setTitle: @"Done" forState:UIControlStateNormal];
    [_tableView setEditing:YES animated:YES];
    _editing = YES;
  }
}

- (void)showErrorMessage:(NSString *)message {
 
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
  CGPoint point = [gestureRecognizer locationInView:_tableView];
  NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:point];
  if (indexPath) {
    GCKMediaQueueItem *item = [_mediaClient.mediaStatus queueItemAtIndex:indexPath.row];
    if (item) {
      [self startRequest:[_mediaClient queueJumpToItemWithID:item.itemID]];
    }
  }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_mediaClient.mediaStatus.queueItemCount == 0) {
        return 0;
    }
   return _mediaClient.mediaStatus.queueItemCount+1;
}
 
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (indexPath.row == _mediaClient.mediaStatus.queueItemCount) {
         UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MediaLastCell"];
            return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MediaCell"];
    GCKMediaQueueItem *item = [_mediaClient.mediaStatus queueItemAtIndex:indexPath.row];
    
    NSString *title = [item.mediaInformation.metadata stringForKey:kGCKMetadataKeyTitle];
    NSString *streamDuration = [GCKUIUtils timeIntervalAsString:item.mediaInformation.streamDuration];
    NSString *detail = [NSString stringWithFormat:@"Duration: %@", streamDuration];
    
    UILabel *mediaTitle = (UILabel *)[cell viewWithTag:1];
    mediaTitle.text = title;
    
    UILabel *mediaOwner = (UILabel *)[cell viewWithTag:2];
    mediaOwner.text = detail;

  if (_mediaClient.mediaStatus.currentItemID == item.itemID) {
    selecatedMediaTitle = [item.mediaInformation.metadata stringForKey:kGCKMetadataKeyTitle];
    cell.backgroundColor = [UIColor lightGrayColor];
    cell.contentView.backgroundColor = [UIColor lightGrayColor];
    self.ibSeekSlider.maximumValue = item.mediaInformation.streamDuration;
    self.ibSeekSlider.value = _mediaClient.mediaStatus.streamPosition;
    self.ibCurrentPlayingMediaTitle.text = title;
    selectedIndex = indexPath.row;
  } else {
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
  }

  UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:3];

  NSArray *images = item.mediaInformation.metadata.images;
  if (images && images.count > 0) {
    GCKImage *image = images[0];

    [[GCKCastContext sharedInstance]
            .imageCache fetchImageForURL:image.URL
                              completion:^(UIImage *image) {
                                imageView.image = image;
                                [cell setNeedsLayout];
                              }];
  }

  return cell;
}

#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView
    canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
  return YES;
}

- (void)tableView:(UITableView *)tableView
    moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath
           toIndexPath:(NSIndexPath *)destinationIndexPath {
  if (sourceIndexPath.row == destinationIndexPath.row) {
    return;
  }
  GCKMediaQueueItem *sourceItem = [_mediaClient.mediaStatus queueItemAtIndex:sourceIndexPath.row];
  NSUInteger insertBeforeID = kGCKMediaQueueInvalidItemID;

  if (destinationIndexPath.row < (NSInteger)[_mediaClient.mediaStatus queueItemCount] - 1) {
    GCKMediaQueueItem *beforeItem = [_mediaClient.mediaStatus queueItemAtIndex:destinationIndexPath.row];
    insertBeforeID = beforeItem.itemID;
  }
  
  [self startRequest:[_mediaClient queueMoveItemWithID:sourceItem.itemID
                                      beforeItemWithID:insertBeforeID]];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
  return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView
    commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
     forRowAtIndexPath:(NSIndexPath *)indexPath {
  if (editingStyle == UITableViewCellEditingStyleDelete) {
    // Delete row.
    GCKMediaQueueItem *item = [_mediaClient.mediaStatus queueItemAtIndex:indexPath.row];
    if (item) {
      [self startRequest:[_mediaClient queueRemoveItemWithID:item.itemID]];
    }
  }
}

- (void)tableView:(UITableView *)tableView
    didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath {
  // No-op.
}

#pragma mark - Session handling

- (void)attachToCastSession:(GCKCastSession *)castSession {
  _mediaClient = castSession.remoteMediaClient;
  [_mediaClient addListener:self];
  [_tableView reloadData];
}

- (void)detachFromCastSession {
  [_mediaClient removeListener:self];
  _mediaClient = nil;
  [_tableView reloadData];
}

#pragma mark - GCKSessionManagerListener

- (void)sessionManager:(GCKSessionManager *)sessionManager
   didStartCastSession:(GCKCastSession *)session {
  [self attachToCastSession:session];
     NSLog(@"didStartCastSession");
}

- (void)sessionManager:(GCKSessionManager *)sessionManager
 didSuspendCastSession:(GCKCastSession *)session
            withReason:(GCKConnectionSuspendReason)reason {
  [self detachFromCastSession];
      NSLog(@"didSuspendCastSession");
}

- (void)sessionManager:(GCKSessionManager *)sessionManager
  didResumeCastSession:(GCKCastSession *)session {
  [self attachToCastSession:session];
       NSLog(@"didResumeCastSession");
}

- (void)sessionManager:(GCKSessionManager *)sessionManager
    willEndCastSession:(GCKCastSession *)session {
    [self detachFromCastSession];
         NSLog(@"willEndCastSession");
}

#pragma mark - GCKRemoteMediaClientListener

- (void)remoteMediaClient:(GCKRemoteMediaClient *)client
     didUpdateMediaStatus:(GCKMediaStatus *)mediaStatus {
    [_tableView reloadData];
    
    if (mediaStatus.playerState == GCKMediaPlayerStateIdle) {
        if (mediaStatus.idleReason == GCKMediaPlayerIdleReasonFinished) {
            if ([self getIndexOfTitle] != -1) {
                if (self.mediaList.rootItem.children.count > [self getIndexOfTitle]) {
                    selectedItem = (MediaItem *)(self.mediaList.rootItem.children)[ [self getIndexOfTitle]+1];
                    [self loadSelectedItemByAppending:NO];
                }
            }
        }
    }
    self.ibSeekSlider.value = mediaStatus.streamPosition;
    NSLog(@"%f", mediaStatus.streamPosition);
    NSLog(@"remoteMediaClient");
}

-(int)getIndexOfTitle{
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.mediaList = delegate.mediaList;
    int index = -1;
    for (MediaItem *mediaIteam in self.mediaList.rootItem.children) {
        index++;
        if ([selecatedMediaTitle isEqualToString:mediaIteam.title]) {
            return index;
        }
    }
    return -1;
}

- (void)remoteMediaClientDidUpdateQueue:(GCKRemoteMediaClient *)client {
  [_tableView reloadData];
      NSLog(@"remoteMediaClientDidUpdateQueue");
}

#pragma mark - Request scheduling

- (void)startRequest:(GCKRequest *)request {
  _queueRequest = request;
  _queueRequest.delegate = self;
  _tableView.userInteractionEnabled = NO;
}

#pragma mark - GCKRequestDelegate

- (void)requestDidComplete:(GCKRequest *)request {
  if (request == _queueRequest) {
    _queueRequest = nil;
    _tableView.userInteractionEnabled = YES;
  }
}

- (void)request:(GCKRequest *)request didFailWithError:(GCKError *)error {
  if (request == _queueRequest) {
    _queueRequest = nil;
    _tableView.userInteractionEnabled = YES;
    [self showErrorMessage:[NSString
                               stringWithFormat:@"Queue request failed:\n%@",
                                                error.description]];
  }
}

- (void)requestWasReplaced:(GCKRequest *)request {
  if (request == _queueRequest) {
    _queueRequest = nil;
    _tableView.userInteractionEnabled = YES;
  }
}

-(void)loadSelectedItemByAppending:(BOOL)appending {
    
    GCKSession *session = [GCKCastContext sharedInstance].sessionManager.currentSession;
    if ([session isKindOfClass:[GCKCastSession class]]) {
        GCKCastSession *castSession = (GCKCastSession *)session;
        if (castSession.remoteMediaClient) {
            GCKMediaQueueItemBuilder *builder = [[GCKMediaQueueItemBuilder alloc] init];
            builder.mediaInformation = selectedItem.mediaInfo;
            builder.autoplay = YES;
            builder.preloadTime = [[NSUserDefaults standardUserDefaults] integerForKey:@"preload_time_sec"];
            GCKMediaQueueItem *item = [builder build];
            if (castSession.remoteMediaClient.mediaStatus && appending) {
                NSLog(@"Count: %lu",castSession.remoteMediaClient.mediaStatus.queueItemCount);
                GCKRequest *request = [castSession.remoteMediaClient queueInsertItem:item
                                                                    beforeItemWithID:kGCKMediaQueueInvalidItemID];
                request.delegate = self;
            } else {
                GCKMediaRepeatMode repeatMode = castSession.remoteMediaClient.mediaStatus ? castSession.remoteMediaClient.mediaStatus.queueRepeatMode : GCKMediaRepeatModeOff;
                
                GCKRequest *request = [castSession.remoteMediaClient queueLoadItems:@[ item ]
                                                                         startIndex:0
                                                                       playPosition:0
                                                                         repeatMode:repeatMode
                                                                         customData:nil];
                request.delegate = self;
            }
        }
    }
}

#pragma mark - Player Control Button

- (IBAction)didTapOnSeekBackwoards:(id)sender {
    GCKCastSession *castSession = [GCKCastContext sharedInstance].sessionManager.currentCastSession;
    if ([castSession isKindOfClass:[GCKCastSession class]]) {
        if (castSession.remoteMediaClient) {
            GCKMediaSeekOptions *seek = [[GCKMediaSeekOptions alloc] init];
            seek.relative = YES;
            seek.resumeState = GCKMediaResumeStatePlay;
            seek.interval = -15;
            [castSession.remoteMediaClient seekWithOptions:seek];
 
        }
    }
}

- (IBAction)didTapOnSeekForward:(id)sender {
    GCKCastSession *castSession = [GCKCastContext sharedInstance].sessionManager.currentCastSession;
    if ([castSession isKindOfClass:[GCKCastSession class]]) {
        if (castSession.remoteMediaClient) {
            GCKMediaSeekOptions *seek = [[GCKMediaSeekOptions alloc] init];
            seek.relative = YES;
            seek.resumeState = GCKMediaResumeStatePause;
            seek.interval = 15000;
            [castSession.remoteMediaClient seekWithOptions:seek];
 
        }
    }
}

- (IBAction)didTapOnPauseButton:(id)sender {
    GCKCastSession *castSession = [GCKCastContext sharedInstance].sessionManager.currentCastSession;
    if ([castSession isKindOfClass:[GCKCastSession class]]) {
        if (castSession.remoteMediaClient) {
            if (self.ibPauseBtn.selected) {
                [castSession.remoteMediaClient play];
                [self.ibPauseBtn setSelected:NO];
            }else{
                [castSession.remoteMediaClient pause];
                [self.ibPauseBtn setSelected:YES];
            }
        }
    }
}

- (IBAction)didTapOnNextButton:(id)sender {
    if ([_mediaClient.mediaStatus queueHasNextItem]) {
        GCKMediaQueueItem *item = [_mediaClient.mediaStatus nextQueueItem];
        [self startRequest:[_mediaClient queueJumpToItemWithID:item.itemID]];
    }
}

- (IBAction)didTapOnPrevButton:(id)sender {
    if ([_mediaClient.mediaStatus queueHasPreviousItem]) {
        GCKMediaQueueItem *item = [_mediaClient.mediaStatus currentQueueItem];
        [self startRequest:[_mediaClient queueJumpToItemWithID:item.itemID-1]];
    }
}

@end
