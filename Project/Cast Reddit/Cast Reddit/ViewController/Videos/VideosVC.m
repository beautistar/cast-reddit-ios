//
//  VideosVC.m
//  
//
//  Created by praveen on 1/16/18.
//

#import "VideosVC.h"
#import "CRHomeCell.h"
#import "MMAlertView.h"
#import "NetworkManager.h"
#import "UIImageView+AFNetworking.h"
#import <GoogleCast/GoogleCast.h>

@interface VideosVC ()<UITableViewDataSource, UITableViewDelegate, CellDelegate>{
        NSMutableArray *listOfPopular;
}

@end

@implementation VideosVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.ibScrollLbl.marqueeType = MLContinuous;
    self.ibScrollLbl.scrollDuration = 10.0;
    self.ibScrollLbl.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    self.ibScrollLbl.fadeLength = 10.0f;
    self.ibScrollLbl.leadingBuffer = 0.0f;
    self.ibScrollLbl.trailingBuffer = 0.0f;
    NSString *strings = @"Videos | GIFS | Pics | Favorties | Subreddit  ";
    self.ibScrollLbl.text = strings;
    self.navigationItem.title = _loadCat;
    [self apiCallToRedditServer];
}

-(void)apiCallToRedditServer{
    listOfPopular = [[NSMutableArray alloc] init];
    NSString *url = [NSString stringWithFormat:@"https://www.reddit.com/r/%@/hot.json?show=all&limit=50", _loadCat];
    [[NetworkManager sharedInstance] performNetworkOperationsWith:nil andBaseUrl:url showLoading:YES success:^(NSDictionary *responseObject) {
        listOfPopular = [[responseObject objectForKey:@"data"] objectForKey:@"children"];
        [self.ibTableView reloadData];
        
    } failure:^(NSError *failure) {
        
        NSArray *items =@[MMItemMake(@"Cancel", MMItemTypeNormal, nil),
                          MMItemMake(@"Ok", MMItemTypeNormal, nil)];
        [[[MMAlertView alloc] initWithTitle:@"Error"
                                     detail:@"Please try later or your internet."
                                      items:items]
         showWithBlock:nil];
        
    }];
}

- (IBAction)didTapOnBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listOfPopular.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CRHomeCell *cell = (CRHomeCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.delegate = self;
    
    
    NSDictionary *userDetails = [[listOfPopular objectAtIndex:indexPath.row] objectForKey:@"data"];
    
    cell.ibUserName.text = [NSString stringWithFormat:@"By %@",[userDetails objectForKey:@"author"]];
    
    cell.ibLikesCountLbl.text =  [NSString stringWithFormat:@"%d",[[userDetails objectForKey:@"score"] intValue]];
    cell.ibCommentsCountLbl.text = [NSString stringWithFormat:@"%d",[[userDetails objectForKey:@"num_comments"] intValue]];
    cell.ibVideoTitleLbl.text = [userDetails objectForKey:@"title"];
    
    double unixTimeStamp = [[userDetails objectForKey:@"created"] doubleValue];
    NSTimeInterval _interval=unixTimeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSTimeInterval seconds = [[NSDate date] timeIntervalSinceDate:date];
    int hours = seconds/3600.0;
    if (hours < 0) {
        hours = 0;
    }
    cell.ibHourseLbl.text = [NSString stringWithFormat:@"%dh",hours];
    
//
//    NSURL *url = [NSURL URLWithString:[userDetails objectForKey:@"icon_img"]];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    UIImage *placeholderImage = [UIImage imageNamed:@"test"];
//
//    __weak CRHomeCell *weakCell = cell;
//    [cell.ibThumbImage setImageWithURLRequest:request
//                             placeholderImage:placeholderImage
//                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
//                                          weakCell.ibThumbImage.image = image;
//                                          NSLog(@"------------");
//                                      } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
//                                          NSLog(@"sdfsdgsdgdfsgdfdfgdfhgfh");
//                                      }];
    
    
    return cell;
}
- (IBAction)didTapOnHomeBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(IBAction)didTapVideoTypesBtn:(id)sender{
    NSArray *items =@[MMItemMake(@"Hot Videos", MMItemTypeNormal, nil),
                      MMItemMake(@"New Videos", MMItemTypeNormal, nil),
                      MMItemMake(@"Top Videos", MMItemTypeNormal, nil),
                      MMItemMake(@"Controversial Videos", MMItemTypeNormal, nil)];
    
    [[[MMAlertView alloc] initWithTitle:@"Sorting"
                                 detail:@""
                                  items:items]
     showWithBlock:nil];
}

- (IBAction)didTapOnconnectChromeCast:(id)sender {
    NSArray *items =@[MMItemMake(@"Disconnect", MMItemTypeNormal, nil),
                      MMItemMake(@"Connect", MMItemTypeNormal, nil)];
    
    [[[MMAlertView alloc] initWithTitle:@"Chromecast"
                                 detail:@"Do you want connect chromecost?"
                                  items:items]
     showWithBlock:nil];
    
}

- (void)didTapTitleText:(CRHomeCell *)cell{
    NSArray *items =@[MMItemMake(@"Hot", MMItemTypeNormal, nil),
                      MMItemMake(@"New", MMItemTypeNormal, nil),
                      MMItemMake(@"Top", MMItemTypeNormal, nil),
                      MMItemMake(@"Controversial", MMItemTypeNormal, nil)];
    
    [[[MMAlertView alloc] initWithTitle:@"Sort videos By"
                                 detail:@""
                                  items:items]
     showWithBlock:nil];
}
- (void)didTapOnMoreBtn:(CRHomeCell *)cell{
    UIActivityViewController *controller =[[UIActivityViewController alloc] initWithActivityItems:@[@"", @""] applicationActivities:nil];
    controller.excludedActivityTypes = @[
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         ];
    
    [self presentViewController:controller animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
