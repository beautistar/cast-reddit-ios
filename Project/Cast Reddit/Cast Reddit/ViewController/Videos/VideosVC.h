//
//  VideosVC.h
//  
//
//  Created by praveen on 1/16/18.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"

@interface VideosVC : UIViewController

@property (weak, nonatomic) IBOutlet MarqueeLabel *ibScrollLbl;
@property (weak, nonatomic) IBOutlet UITableView *ibTableView;

@property (weak, nonatomic) NSString *loadCat;


@end
