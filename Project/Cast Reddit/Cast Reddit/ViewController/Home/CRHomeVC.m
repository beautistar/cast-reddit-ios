//
//  CRHomeVC.m
//  Cast Reddit
//
//  Created by praveen on 1/16/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import "CRHomeVC.h"
#import "CRHomeCell.h"
#import "MMAlertView.h"
#import "MMSheetView.h"
#import "TLYShyNavBarManager.h"
#import "NetworkManager.H"
#import "DmainUrls.h"
#import "UIImageView+AFNetworking.h"


#import "ActionSheet.h"
#import <GoogleCast/GoogleCast.h>
#import "AppDelegate.h"
#import "MediaItem.h"
#import "MediaListModel.h"
#import "Toast.h"
#import "MediaViewController.h"

#import "ActionSheetFilter.h"
#import "ActionSheetSorting.h"
#import "ActionSheetPlayer.h"

static NSString *const kPrefMediaListURL = @"media_list_url";

@interface CRHomeVC ()<UITableViewDataSource, UITableViewDelegate,CellDelegate, GCKSessionManagerListener, GCKRequestDelegate, MediaListModelDelegate,ActionSheetPlayerDelegate, ActionSheetSortingDelegate,FilterDelegate,GCKRemoteMediaClientListener>{
   
    NSString *sortBy;
    NSString *filterBy;

    NSInteger selectedIndex;
    NSMutableArray *listOfPopular;
    
    GCKSessionManager *_sessionManager;
    GCKRemoteMediaClient *_mediaClient;
    GCKUICastButton *_castButton;
    MediaItem *selectedItem;
    ActionSheet *_actionSheet;
    
    UIBarButtonItem *_queueButton;
    NSString *afterString;
    BOOL _queueAdded;
}

@property(nonatomic) MediaListModel *mediaList;

@end



@implementation CRHomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    listOfPopular = [[NSMutableArray alloc] init];
    sortBy = @"hot";
    filterBy = @"videos";
    [self loadDataFromServer:NO];
    [self addNavigationBarButtons];
    [self setThumbslider];
}
-(void)setThumbslider{
    CGRect thumbRect = CGRectMake(0,0,1,1); UIGraphicsBeginImageContextWithOptions(CGSizeMake(1,1), false, 0);
    [[UIColor clearColor] setFill]; UIRectFill(thumbRect);
    UIImage* blankImg = UIGraphicsGetImageFromCurrentImageContext(); UIGraphicsEndImageContext( );
    [self.ibTimeSlider setThumbImage:blankImg forState:UIControlStateNormal];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setpScrolling];
    self.ibPauseBtn.hidden = YES;
    _sessionManager = [GCKCastContext sharedInstance].sessionManager;
    [_sessionManager addListener:self];
    [self addObserverNotification];
    if (_sessionManager.hasConnectedCastSession) {
        [self attachToCastSession:_sessionManager.currentCastSession];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [_mediaClient removeListener:self];
}

-(void)addObserverNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deviceOrientationDidChange:) name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(castDeviceDidChange:) name:kGCKCastStateDidChangeNotification
                                               object:[GCKCastContext sharedInstance]];
}

-(void)addNavigationBarButtons{
    // Cast Button
    _castButton =  [[GCKUICastButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    _castButton.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_castButton];
    
    //Queue Button
    _queueButton = [[UIBarButtonItem alloc]
                    initWithImage:[UIImage imageNamed:@"playlist_white.png"]
                    style:UIBarButtonItemStylePlain
                    target:self
                    action:@selector(didTapQueueButton:)];
}

-(void)loadDataFromServer:(BOOL)after{
    NSString *url = [NSString stringWithFormat:@"https://www.reddit.com/r/%@/%@.json?show=all&limit=50",filterBy,sortBy];
    if (after) {
       url = [NSString stringWithFormat:@"https://www.reddit.com/r/%@/%@.json?show=all&limit=50&after=%@",filterBy,sortBy,afterString];
    }
    
    [[NetworkManager sharedInstance] performNetworkOperationsWith:nil
                                                       andBaseUrl:url
                                                      showLoading:NO
                                                          success:^(NSDictionary *responseObject) {
                                                              
        NSArray *array = [[responseObject objectForKey:@"data"] objectForKey:@"children"];
        
        for (NSDictionary *dic in array) {
            if (![[dic objectForKey:@"data"] isKindOfClass:[NSNull class]] && [filterBy isEqualToString: @"videos"]) {
                if (![[[dic objectForKey:@"data"]  objectForKey:@"media"] isKindOfClass:[NSNull class]] ) {
                    NSDictionary *data = [[dic objectForKey:@"data"]  objectForKey:@"media"];
                    if (![data isKindOfClass:[NSNull class]]){
                        if ([[[[dic objectForKey:@"data"]  objectForKey:@"media"] objectForKey:@"type"] isEqualToString: @"youtube.com"]){
                            [listOfPopular addObject:[dic objectForKey:@"data"]];
                        }
                    }
                }
            }else if (![[dic objectForKey:@"data"] isKindOfClass:[NSNull class]]){
                [listOfPopular addObject:[dic objectForKey:@"data"]];
            }
        }
                                                              
        if ([filterBy isEqualToString: @"videos"]) {
            [self loadMediaList];
        }
        afterString = [[responseObject objectForKey:@"data"] objectForKey:@"after"];
        [self.ibTableView reloadData];
        
    } failure:^(NSError *failure) {
        NSArray *items =@[MMItemMake(@"Cancel", MMItemTypeNormal, nil),
                          MMItemMake(@"Ok", MMItemTypeNormal, nil)];
        [[[MMAlertView alloc] initWithTitle:@"Error"
                                     detail:@"Please try later or your internet."
                                      items:items]
         showWithBlock:nil];
    }];
}


-(void)setpScrolling{
    self.ibScrollLbl.marqueeType = MLContinuous;
    self.ibScrollLbl.scrollDuration = 10.0;
    self.ibScrollLbl.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    self.ibScrollLbl.fadeLength = 10.0f;
    self.ibScrollLbl.leadingBuffer = 0.0f;
    self.ibScrollLbl.trailingBuffer = 0.0f;
    NSString *strings = @"Videos | GIFS | Pics | Favorties | Subreddit  ";
    self.ibScrollLbl.text = strings;
    
    self.shyNavBarManager.scrollView = self.ibTableView;
    [self.shyNavBarManager setExtensionView:_ibTopView];
}

- (void)deviceOrientationDidChange:(NSNotification *)notification {
 
}

- (void)castDeviceDidChange:(NSNotification *)notification {
    if ([GCKCastContext sharedInstance].castState != GCKCastStateNoDevicesAvailable) {
        // You can present the instructions on how to use Google Cast on
        // the first time the user uses you app
        [[GCKCastContext sharedInstance] presentCastInstructionsViewControllerOnce];
        _castButton.tintColor = [UIColor colorWithRed:39.0/255.0 green:182.0/255.0 blue:232.0/255.0 alpha:1.0];
    }
}

- (void)playSelectedItemRemotely {
    [self loadSelectedItemByAppending:NO];
}

- (void)enqueueSelectedItemRemotely {
    [self loadSelectedItemByAppending:YES];
    NSString *message = [NSString stringWithFormat:@"Added \"%@\" to queue.",[selectedItem.mediaInfo.metadata stringForKey:kGCKMetadataKeyTitle]];
    [Toast displayToastMessage:message
               forTimeInterval:3
                        inView:[UIApplication sharedApplication].delegate.window];
   [self setQueueButtonVisible:YES];
}

- (void)setQueueButtonVisible:(BOOL)visible {
    if (visible && !_queueAdded) {
        NSMutableArray *barItems = [[NSMutableArray alloc]
                                    initWithArray:self.navigationItem.rightBarButtonItems];
        [barItems addObject:_queueButton];
        self.navigationItem.rightBarButtonItems = barItems;
        _queueAdded = YES;
    } else if (!visible && _queueAdded) {
        NSMutableArray *barItems = [[NSMutableArray alloc]
                                    initWithArray:self.navigationItem.rightBarButtonItems];
        [barItems removeObject:_queueButton];
        self.navigationItem.rightBarButtonItems = barItems;
        _queueAdded = NO;
    }
}

-(void)loadSelectedItemByAppending:(BOOL)appending {

    GCKSession *session = [GCKCastContext sharedInstance].sessionManager.currentSession;
    if ([session isKindOfClass:[GCKCastSession class]]) {
        GCKCastSession *castSession = (GCKCastSession *)session;
        if (castSession.remoteMediaClient) {
            GCKMediaQueueItemBuilder *builder = [[GCKMediaQueueItemBuilder alloc] init];
            builder.mediaInformation = selectedItem.mediaInfo;
            builder.autoplay = YES;
            builder.preloadTime = [[NSUserDefaults standardUserDefaults] integerForKey:@"preload_time_sec"];
            GCKMediaQueueItem *item = [builder build];
            if (castSession.remoteMediaClient.mediaStatus && appending) {
                NSLog(@"Count: %lu",castSession.remoteMediaClient.mediaStatus.queueItemCount);
                GCKRequest *request = [castSession.remoteMediaClient queueInsertItem:item
                                                                    beforeItemWithID:kGCKMediaQueueInvalidItemID];
                request.delegate = self;
            } else {
                GCKMediaRepeatMode repeatMode = castSession.remoteMediaClient.mediaStatus ? castSession.remoteMediaClient.mediaStatus.queueRepeatMode : GCKMediaRepeatModeOff;
                
                GCKRequest *request = [castSession.remoteMediaClient queueLoadItems:@[ item ]
                                                                         startIndex:0
                                                                       playPosition:0
                                                                         repeatMode:repeatMode
                                                                         customData:nil];
                request.delegate = self;
            }
        }
    }
}

#pragma mark - GCKSessionManagerListener

- (void)sessionManager:(GCKSessionManager *)sessionManager
   didStartCastSession:(GCKCastSession *)session {
    [self attachToCastSession:session];
    NSLog(@"didStartCastSession");
}

- (void)sessionManager:(GCKSessionManager *)sessionManager
 didSuspendCastSession:(GCKCastSession *)session
            withReason:(GCKConnectionSuspendReason)reason {
    [self detachFromCastSession];
    NSLog(@"didSuspendCastSession");
}

- (void)sessionManager:(GCKSessionManager *)sessionManager
  didResumeCastSession:(GCKCastSession *)session {
    [self attachToCastSession:session];
    NSLog(@"didResumeCastSession");
}

- (void)sessionManager:(GCKSessionManager *)sessionManager
    willEndCastSession:(GCKCastSession *)session {
    [self detachFromCastSession];
    NSLog(@"willEndCastSession");
}

#pragma mark - GCKSessionManagerListener

- (void)sessionManager:(GCKSessionManager *)sessionManager didStartSession:(GCKSession *)session {
    NSLog(@"MediaViewController: sessionManager didStartSession %@", session);
    [self setQueueButtonVisible:YES];
 
}

- (void)sessionManager:(GCKSessionManager *)sessionManager didResumeSession:(GCKSession *)session {
    NSLog(@"MediaViewController: sessionManager didResumeSession %@", session);
      [self setQueueButtonVisible:YES];
}

- (void)sessionManager:(GCKSessionManager *)sessionManager
         didEndSession:(GCKSession *)session
             withError:(NSError *)error {
    NSLog(@"session ended with error: %@", error);
    NSString *message = [NSString stringWithFormat:@"The Casting session has ended.\n%@",error.description];
    
    [self setQueueButtonVisible:NO];
    
    [Toast displayToastMessage:message
               forTimeInterval:3
                        inView:[UIApplication sharedApplication].delegate.window];
}

- (void)sessionManager:(GCKSessionManager *)sessionManager didFailToStartSessionWithError:(NSError *)error {
    NSArray *items =@[MMItemMake(@"Cancel", MMItemTypeNormal, nil),
                      MMItemMake(@"Ok", MMItemTypeNormal, nil)];
    
    [[[MMAlertView alloc] initWithTitle:@"Failed to start a session"
                                 detail:error.description
                                  items:items]
     showWithBlock:nil];
      [self setQueueButtonVisible:NO];
 
}

- (void)sessionManager:(GCKSessionManager *)sessionManager didFailToResumeSession:(GCKSession *)session
             withError:(NSError *)error {
    [Toast displayToastMessage:@"The Casting session could not be resumed."
               forTimeInterval:3
                        inView:[UIApplication sharedApplication].delegate.window];
      [self setQueueButtonVisible:NO];
 
}

#pragma mark - GCKRemoteMediaClientListener
-(void)remoteMediaClient:(GCKRemoteMediaClient *)client didUpdateMediaStatus:(GCKMediaStatus *)mediaStatus {
    NSLog(@"HomeVC Delegate remoteMediaClient didUpdateMediaStatus");
    if (mediaStatus.playerState == GCKMediaPlayerStateIdle) {
        if (mediaStatus.idleReason == GCKMediaPlayerIdleReasonFinished) {
            int index = (int)[self.rootItem.children indexOfObject:selectedItem];
            if (self.rootItem.children.count > index+1) {
                selectedIndex = index+1;
                selectedItem = (MediaItem *)(self.rootItem.children)[index+1];
                [self didTapPlayVideo];
            }
        }
    }else if (mediaStatus.playerState == GCKMediaPlayerStatePlaying){
        self.ibTimeSlider.maximumValue = mediaStatus.currentQueueItem.mediaInformation.streamDuration;
        self.ibTimeSlider.value = mediaStatus.streamPosition;
    }
    GCKMediaQueueItem *item = [client.mediaStatus currentQueueItem];
    self.ibMediaTitleLbl.text = [item.mediaInformation.metadata stringForKey:kGCKMetadataKeyTitle];
}

- (void)remoteMediaClientDidUpdateQueue:(GCKRemoteMediaClient *)client {
    NSLog(@"remoteMediaClientDidUpdateQueue %@",client);
}
#pragma mark - Session handling

- (void)attachToCastSession:(GCKCastSession *)castSession {
    _mediaClient = castSession.remoteMediaClient;
    [_mediaClient addListener:self];
}

- (void)detachFromCastSession {
//    [_mediaClient removeListener:self];
//    _mediaClient = nil;

}

#pragma mark - ActionSheetPlayerDelegate

- (void)didTapPlayVideo{
    BOOL hasConnectedCastSession = [GCKCastContext sharedInstance].sessionManager.hasConnectedCastSession;
    if (selectedItem.mediaInfo && hasConnectedCastSession) {
        [self playSelectedItemRemotely];
        self.ibPauseBtn.hidden = NO;
        self.ibMediaTitleLbl.text = selectedItem.title;
    }
}

-(void)didTapOnAddtoQ{
    BOOL hasConnectedCastSession = [GCKCastContext sharedInstance].sessionManager.hasConnectedCastSession;
    if (selectedItem.mediaInfo && hasConnectedCastSession) {
        [self enqueueSelectedItemRemotely];
    }
}
-(void)didTapOnPlayNext{
    BOOL hasConnectedCastSession = [GCKCastContext sharedInstance].sessionManager.hasConnectedCastSession;
    if (selectedItem.mediaInfo && hasConnectedCastSession) {
        [self enqueueSelectedItemRemotely];
    }
}

#pragma mark - didTapOnButtons

- (IBAction)didTapOnconnectChromeCast:(id)sender {
    
    NSArray *items =@[MMItemMake(@"Disconnect", MMItemTypeNormal, nil),
                      MMItemMake(@"Connect", MMItemTypeNormal, nil)];
    
    [[[MMAlertView alloc] initWithTitle:@"Chromecast"
                                 detail:@"Do you want connect chromecost?"
                                  items:items]
     showWithBlock:nil];
    
}
- (void)didTapPlayVideo:(CRHomeCell *)cell{
    
    NSIndexPath *index = [self.ibTableView indexPathForCell:cell];
    if (self.rootItem.children.count <= index.row) {
        [self showAlertWithTitle:@"Please Wait" message:@"Video is loading from server..."];
    }else{
        ActionSheetPlayer *innerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ActionSheetPlayer"];
        innerViewController.view.frame = self.view.bounds;
        innerViewController.delegate = self;
        [self addChildViewController:innerViewController];
        [self.view addSubview:innerViewController.view];
        [innerViewController didMoveToParentViewController:self];
        
        selectedIndex = index.row;
        selectedItem = (MediaItem *)(self.rootItem.children)[index.row];
    }
}


-(IBAction)didTapOnPauseBtn:(id)sender{
    GCKCastSession *castSession = [GCKCastContext sharedInstance].sessionManager.currentCastSession;
     if ([castSession isKindOfClass:[GCKCastSession class]]) {
         if (castSession.remoteMediaClient) {
             NSLog(@"self.ibPauseBtn: %d",self.ibPauseBtn.selected);
             if (self.ibPauseBtn.selected) {
                 [castSession.remoteMediaClient pause];
                 [self.ibPauseBtn setSelected:NO];
             }else{
                 [castSession.remoteMediaClient play];
                 [self.ibPauseBtn setSelected:YES];
             }
         }
     }
}

-(IBAction)didTapFilterBtn:(id)sender{
    ActionSheetFilter *innerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ActionSheetFilter"];
     innerViewController.view.frame = self.view.bounds;
    innerViewController.delegate = self;
    [self addChildViewController:innerViewController];
    [self.view addSubview:innerViewController.view];
    [innerViewController didMoveToParentViewController:self];
}
- (void)filterSelecatedType:(NSString *)type{
    filterBy = type;
    [self removeListAllIteam];
}

-(void)removeListAllIteam{
    [listOfPopular removeAllObjects];
    [self.ibTableView reloadData];
    [self.rootItem.children removeAllObjects];
    self.mediaList.delegate = nil;
    [self loadDataFromServer:NO];
}

-(IBAction)didTapVideoTypesBtn:(id)sender{
    ActionSheetSorting *innerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ActionSheetSorting"];
    innerViewController.view.frame = self.view.bounds;
    innerViewController.delegate = self;
    [self addChildViewController:innerViewController];
    [self.view addSubview:innerViewController.view];
    [innerViewController didMoveToParentViewController:self];
}

- (void)didTapOnVideoType:(NSString *)type{
    sortBy = type;
    [self removeListAllIteam];
}

- (void)didTapTitleText:(CRHomeCell *)cell{
    NSArray *items =@[MMItemMake(@"Play now", MMItemTypeNormal, nil),
                      MMItemMake(@"Play next", MMItemTypeNormal, nil),
                      MMItemMake(@"Add to Q", MMItemTypeNormal, nil)];
    
    [[[MMAlertView alloc] initWithTitle:@""
                                 detail:@""
                                  items:items]
     showWithBlock:nil];
}
- (void)didTapOnMoreBtn:(CRHomeCell *)cell{
    NSIndexPath *index = [self.ibTableView indexPathForCell:cell];
    NSString *link = [[listOfPopular objectAtIndex:index.row] objectForKey:@"permalink"];
    NSString *redditLink = [NSString stringWithFormat:@"https://www.reddit.com%@",link];
    UIActivityViewController *controller =[[UIActivityViewController alloc] initWithActivityItems:@[redditLink] applicationActivities:nil];
    controller.excludedActivityTypes = @[
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         ];
    
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - Q Buttons Events

- (void)didTapQueueButton:(id)sender {
    UIViewController *dstVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaQueueViewController"];
    [self.navigationController presentViewController:dstVC animated:YES completion:nil];
}

- (IBAction)didTapQueueBtn:(id)sender {
    UIViewController *dstVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaQueueViewController"];
    [self.navigationController presentViewController:dstVC animated:YES completion:nil];
}

#pragma mark - UITableView-Delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == listOfPopular.count-1) {
        [self loadDataFromServer:YES];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listOfPopular.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // Cell121
    CRHomeCell *cell;

    NSDictionary *userDetails = [listOfPopular objectAtIndex:indexPath.row];
    NSString *stringUrl ;
    if ([filterBy isEqualToString:@"videos"]) {
        stringUrl = [[[userDetails objectForKey:@"media"] objectForKey:@"oembed"] objectForKey:@"thumbnail_url"];
    }else{
        NSArray *temp = [[userDetails objectForKey:@"preview"] objectForKey:@"images"];
        stringUrl = [[[temp objectAtIndex:0] objectForKey:@"source"] objectForKey:@"url"];
    }
    
    if (stringUrl == nil) {
        cell = (CRHomeCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell121"];
    }else{
        cell = (CRHomeCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    }
    
    cell.delegate = self;
    cell.ibUserName.text = [NSString stringWithFormat:@"By %@",[userDetails objectForKey:@"author"]];
    
    NSNumber *likes = [NSNumber numberWithInt:[[userDetails objectForKey:@"score"] intValue]];
    cell.ibLikesCountLbl.text = [self suffixNumber:likes];
    
    NSNumber *comments = [NSNumber numberWithInt:[[userDetails objectForKey:@"num_comments"] intValue]];
    cell.ibCommentsCountLbl.text = [self suffixNumber:comments];
    
    cell.ibVideoTitleLbl.text = [userDetails objectForKey:@"title"];
    
    double unixTimeStamp = [[userDetails objectForKey:@"created"] doubleValue];
    NSTimeInterval _interval=unixTimeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSTimeInterval seconds = [[NSDate date] timeIntervalSinceDate:date];
    int hours = seconds/3600.0;
    if (hours < 0) {
        hours = 0;
    }
    cell.ibHourseLbl.text = [NSString stringWithFormat:@"%dh",hours];
    
    
    if ([filterBy isEqualToString: @"pics"] || [filterBy isEqualToString: @"gifs"]) {
        cell.ibPlayBtn.hidden = YES;
    }else{
        cell.ibPlayBtn.hidden = NO;
    }
    
    if (stringUrl == nil) {
        return cell;
    }
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    UIImage *placeholderImage = [UIImage imageNamed:@"testdsf"];
    
    
    __weak CRHomeCell *weakCell = cell;
    [cell.ibThumbImage setImageWithURLRequest:request
                             placeholderImage:placeholderImage
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                          if (image != nil) {
                                              weakCell.ibThumbImage.image = image;
                                          }else{
                                              cell.ibPlayBtn.hidden = YES;
                                          }
                                          
                                      } failure:^(NSURLRequest *request, NSHTTPURLResponse * _Nullable response, NSError *error){
                                          cell.ibPlayBtn.hidden = YES;
                                          }];
    return cell;
}



- (void)loadMediaList  {
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.mediaList = [[MediaListModel alloc] init];
    self.mediaList = delegate.mediaList;
    self.mediaList.delegate = self;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        [self.mediaList decodeMediaFromJSON:[listOfPopular mutableCopy]];
    });
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"prepareForSegue");
    if ([segue.identifier isEqualToString:@"mediaDetails"]) {
        MediaViewController *viewController = (MediaViewController *)segue.destinationViewController;
        GCKMediaInformation *mediaInfo = [self getSelectedItem].mediaInfo;
        if (mediaInfo) {
            viewController.mediaInfo = mediaInfo;
        }
    }
}

- (MediaItem *)getSelectedItem {
    MediaItem *item = nil;
    item = (MediaItem *)(self.rootItem.children)[selectedIndex];
    return item;
}

#pragma mark - MediaListModelDelegate

- (void)mediaListModelDidLoad:(MediaListModel *)list {
    self.rootItem = self.mediaList.rootItem;
    NSLog(@"Loaded Media: %@", self.rootItem);
}

- (void)mediaListModel:(MediaListModel *)list didFailToLoadWithError:(NSError *)error{
    NSLog(@"Failed load data");
}

#pragma mark - GCKRequestDelegate

- (void)requestDidComplete:(GCKRequest *)request {
    NSLog(@"request %ld completed", (long)request.requestID);
}

- (void)request:(GCKRequest *)request didFailWithError:(GCKError *)error {
    NSLog(@"request %ld failed with error %@", (long)request.requestID, error);
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    MBProgressHUD  *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = @"Please wait...";
    [hud hideAnimated:YES afterDelay:2];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*)suffixNumber:(NSNumber*)number{
    if (!number)
        return @"";
    
    long long num = [number longLongValue];
    
    int s = ( (num < 0) ? -1 : (num > 0) ? 1 : 0 );
    NSString* sign = (s == -1 ? @"-" : @"" );
    
    num = llabs(num);
    
    if (num < 1000)
        return [NSString stringWithFormat:@"%@%lld",sign,num];
    
    int exp = (int) (log10l(num) / 3.f); //log10l(1000));
    
    NSArray* units = @[@"K",@"M",@"G",@"T",@"P",@"E"];
    
    return [NSString stringWithFormat:@"%@%.1f%@",sign, (num / pow(1000, exp)), [units objectAtIndex:(exp-1)]];
}

@end
