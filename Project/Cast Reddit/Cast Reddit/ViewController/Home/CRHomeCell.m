//
//  CRHomeCell.m
//  Cast Reddit
//
//  Created by praveen on 1/16/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import "CRHomeCell.h"
@implementation CRHomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(IBAction)didTapOnvideoTitleClicked:(id)sender{
    if (self.delegate) {
          [self.delegate didTapTitleText:self];
    }
}
-(IBAction)didTapOnMoreBtn:(id)sender{
    if (self.delegate) {
        [self.delegate didTapOnMoreBtn:self];
    }
}
-(IBAction)didTapPlayVideo:(id)sender{
    if (self.delegate) {
        [self.delegate didTapPlayVideo:self];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
