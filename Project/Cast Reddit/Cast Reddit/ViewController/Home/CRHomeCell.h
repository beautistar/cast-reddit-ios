//
//  CRHomeCell.h
//  Cast Reddit
//
//  Created by praveen on 1/16/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CRHomeCell;

@protocol CellDelegate <NSObject>
@optional
- (void)didTapTitleText:(CRHomeCell *)cell;
- (void)didTapOnMoreBtn:(CRHomeCell *)cell;
- (void)didTapPlayVideo:(CRHomeCell *)cell;
@end

@interface CRHomeCell : UITableViewCell
@property (nonatomic, weak) id <CellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *ibVideoTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *ibUserName;
@property (weak, nonatomic) IBOutlet UILabel *ibLikesCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *ibCommentsCountLbl;
@property (weak, nonatomic) IBOutlet UIImageView *ibThumbImage;
@property (weak, nonatomic) IBOutlet UILabel *ibHourseLbl;
@property (weak, nonatomic) IBOutlet UIButton *ibPlayBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ibcImageHight;
@property (strong, nonatomic) NSString  *primaryLink;
@end
