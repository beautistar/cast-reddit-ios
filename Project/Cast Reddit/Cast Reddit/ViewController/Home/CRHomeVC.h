//
//  CRHomeVC.h
//  Cast Reddit
//
//  Created by praveen on 1/16/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"
#import "MBProgressHUD.h"

@class MediaItem;

@interface CRHomeVC : UIViewController

@property (weak, nonatomic) IBOutlet MarqueeLabel *ibScrollLbl;
@property (weak, nonatomic) IBOutlet UIView *ibTopView;
@property (weak, nonatomic) IBOutlet UITableView *ibTableView;
@property (weak, nonatomic) IBOutlet UILabel *ibMediaTitleLbl;
@property (weak, nonatomic) IBOutlet UISlider *ibTimeSlider;

@property (weak, nonatomic) IBOutlet UIButton *ibPauseBtn;
@property(nonatomic, strong, readwrite) MediaItem *rootItem;

@end
