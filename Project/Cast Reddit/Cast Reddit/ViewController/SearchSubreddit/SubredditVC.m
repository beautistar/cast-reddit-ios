//
//  SubredditVC.m
//  Cast Reddit
//
//  Created by praveen on 1/17/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import "SubredditVC.h"
#import "MMAlertView.h"
#import "NetworkManager.h"
#import "VideosVC.h"
#import "SubRedditCell.h"

@interface SubredditVC ()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate,SubRedditDelegate>{
    NSMutableArray *listArray;
    NSMutableArray *serachArray;
    NSMutableArray *favArray;
    NSString *selecatedSubReddit;
}

@end

@implementation SubredditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    serachArray = [[NSMutableArray alloc] init];
    favArray = [[NSMutableArray alloc]init];
    self.ibScrollLbl.marqueeType = MLContinuous;
    self.ibScrollLbl.scrollDuration = 10.0;
    self.ibScrollLbl.animationCurve = UIViewAnimationOptionCurveEaseInOut;
    self.ibScrollLbl.fadeLength = 10.0f;
    self.ibScrollLbl.leadingBuffer = 0.0f;
    self.ibScrollLbl.trailingBuffer = 0.0f;
    NSString *strings = @"Videos | GIFS | Pics | Favorties | Subreddit  ";
    self.ibScrollLbl.text = strings;
 
    [self loadALLSubredditCat];
    
}

-(void)loadALLSubredditCat{
    listArray = [[NSMutableArray alloc] init];
    NSString *url = [NSString stringWithFormat:@"https://api.reddift.net/all_ranking.json"];
    
    [[NetworkManager sharedInstance] performNetworkOperationsWith:nil
                                                       andBaseUrl:url
                                                      showLoading:YES
                                                          success:^(NSDictionary *responseObject) {
                                                         
                                                              NSArray *unsortedArray = [[responseObject objectForKey:@"data"] objectForKey:@"Subscribers"];
                                                              NSMutableArray *tempArra = [[NSMutableArray alloc] init];
                                                              for (NSDictionary *dic in unsortedArray) {
                                                                  [tempArra addObject:[dic objectForKey:@"title"]];
                                                              }
                                                        
                                                            listArray = [NSMutableArray arrayWithArray:[tempArra sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)]];
                                                              
                                                              [self.ibTableView reloadData];
                                                          } failure:^(NSError *failure) {
                                                              NSArray *items =@[MMItemMake(@"Cancel", MMItemTypeNormal, nil),
                                                                                MMItemMake(@"Ok", MMItemTypeNormal, nil)];
                                                              [[[MMAlertView alloc] initWithTitle:@"Error"
                                                                                           detail:@"Please try later or your internet."
                                                                                            items:items]
                                                               showWithBlock:nil];
                                                          }];
}

- (IBAction)didTapOnconnectChromeCast:(id)sender {
    NSArray *items =@[MMItemMake(@"Disconnect", MMItemTypeNormal, nil),
                      MMItemMake(@"Connect", MMItemTypeNormal, nil)];
    
    [[[MMAlertView alloc] initWithTitle:@"Chromecast"
                                 detail:@"Do you want connect chromecost?"
                                  items:items]
     showWithBlock:nil];
    
}

#pragma Mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_ibSearchTableView == tableView) {
        return serachArray.count;
    }else{
        if (section == 0) {
            return 3;
        }else if(section == 1){
            return favArray.count;
        }else{
            return listArray.count;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_ibSearchTableView == tableView) {
       UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"SearchCell"]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.text = [serachArray objectAtIndex:indexPath.row];
        cell.textLabel.textColor = [UIColor whiteColor];
        return cell;
    }

    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"Cell%ld",(long)indexPath.row]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        SubRedditCell *cell = (SubRedditCell *)[tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"SubRedditCell"]];
        cell.ibTitleLabel.text = [listArray objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        return cell;
    }
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        selecatedSubReddit = [favArray objectAtIndex:indexPath.row];
    }else if (indexPath.section == 2){
        selecatedSubReddit = [listArray objectAtIndex:indexPath.row];
    }
    [self performSegueWithIdentifier:@"VideoView" sender:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (_ibSearchTableView != tableView) {
        if (section == 0) {
            return 0;
        }else{
            return 45;
        }
    }
    return CGFLOAT_MIN;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
     if (_ibSearchTableView != tableView) {
          return 3;
     }
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (_ibSearchTableView != tableView) {
        if(section == 0){
            return @"";
        }
        if(section == 1){
            return @"Favoritis";
        }
        return @"Subscriptions";
    }
    return @"";
}

#pragma Mark - UITableViewCellDelegate
- (void)didTapOnFavBtn:(SubRedditCell *)cell{
    NSIndexPath *index = [self.ibTableView indexPathForCell:cell];
    if (index.section == 1) {
        [favArray removeObjectAtIndex:index.row];
    }else if(index.section == 2){
        [favArray addObject:[listArray objectAtIndex:index.row]];
    }
    [self.ibTableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"VideoView"]) {
        VideosVC *vc = (VideosVC *)segue.destinationViewController;
        vc.loadCat = selecatedSubReddit;
    }
}

- (void)btnTapped:(UIButton *)sender {
    [favArray addObject:[listArray objectAtIndex:sender.tag]];
    [self.ibTableView reloadData];
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return @[@"#",@"$",@"&",@"@",@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];;
}

- (IBAction)didTapOnBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)didTapOnHomeBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    self.ibSearchTableView.hidden = NO;
    return YES;
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
   
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    return YES;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.ibSearchTableView.hidden = YES;
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


@end
