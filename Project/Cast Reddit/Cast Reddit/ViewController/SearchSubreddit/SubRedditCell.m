//
//  SubRedditCell.m
//  Cast Reddit
//
//  Created by praveen on 3/25/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import "SubRedditCell.h"

@implementation SubRedditCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)didTapAddFavBtn:(id)sender {
    if (self.delegate) {
        [self.delegate didTapOnFavBtn:self];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
