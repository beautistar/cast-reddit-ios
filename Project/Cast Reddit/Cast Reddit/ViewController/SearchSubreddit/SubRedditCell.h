//
//  SubRedditCell.h
//  Cast Reddit
//
//  Created by praveen on 3/25/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SubRedditCell;

@protocol SubRedditDelegate <NSObject>

@optional
- (void)didTapOnFavBtn:(SubRedditCell *)cell;
@end

@interface SubRedditCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ibTitleLabel;
@property (nonatomic, weak) id <SubRedditDelegate> delegate;

@end
