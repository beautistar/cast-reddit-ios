//
//  SubredditVC.h
//  Cast Reddit
//
//  Created by praveen on 1/17/18.
//  Copyright © 2018 Yin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"
@interface SubredditVC : UIViewController

@property (weak, nonatomic) IBOutlet MarqueeLabel *ibScrollLbl;
@property (weak, nonatomic) IBOutlet UITableView *ibTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *ibSearchBar;
@property (weak, nonatomic) IBOutlet UITableView *ibSearchTableView;
@end
