//
//  main.m
//  Cast Reddit
//
//  Created by Yin on 14/01/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
